/*Домашнее задание №1:
1. Создать главную функцию run() в которой будет выполняться основной код (цикл)
Так же эта функция должна содержать в себе вызовы всех остальных функций.
2. Сделать функцию для получения случайных чисел (getRndNumber).
Значение каждой переменной, в которую мы записываем, какая выпала кость получать с помощью вызова этой функции
3. Сделать одну функцию которая будет печатать строки (print). Она должна принимать только один аргумент. Строку текста которую надо напечатать.
(если у вас выводите данные не только в div с id result а возможно еще в какой то другой div, тогда функция должна принимать 2 аргумента: id и Строку)
4. Сделать функцию для определения совпадений. (isNumbersEqual). Она должна содержать в себе проверку на совпадение и внутри себя вызывать функцию для печать данных в HTML (print)
5. Сделать функцию для определения разницы. (isBigDifference). Она должна содержать в себе соответствующую проверку и внутри себя вызывать функцию для печать данных в HTML (print)
6. Сделать функцию для вычисления результата total. Функция должна вычислить результат и вернуть его. То есть вернуть строку. Полученное из функции значение необходимо потом напечатать с помощью функции (print)
*/

var myResult = document.getElementById("result");

function getRndNumber() {
	return Math.floor((Math.random() * 6) + 1);
}

function showTextById(text) {
	myResult.innerHTML += text;
}

function isNumbersEqual (first, second) {
	if (first == second){
		var number = first;
		showTextById("<div class='doubleresultdice'>" + "Випав дубль. Число " + number + "</div>");
	}
}

function isBigDifference(first, second) {
	if (first < 3 && second > 4 || first > 3 && second < 4){
		var difference = (first < 3 && second > 4) ? second - first : first - second;
		showTextById("<div class='differenceindice'>" + "Великий разброс між костями. Разниця становить: " + difference + "</div>");
	}
}

function getTotalResult(total) {
	result = (total > 100) ? "<div class='resulttotaldice'>" + "<span class='windice'>" + "Перемога, ви набрали очків " + "<b>" + total + "</b>" + "</span>" + "</div>" : "<div class='resulttotaldice'>" + "<span class='lostdice'>" +"Ви проиграли, у вас очків " + "<b>" + total + "</b>" + "</span>" +"</div>";
	return result;
}

(function run () {
	var total = 0;
	for (var i = 1; i <= 15; i++){
		if (i == 8 || i == 13) {continue;}
		
		var first = getRndNumber();
		var second = getRndNumber();
		
		showTextById ("<div class='resultdice'>" + "Кинули кубіки " + i + " раз" + " " + "<div class='firstdice'>" + " " +"<div class='dice" + first + "'>" + "</div>" + "<div class='dice" + second + "'>" + "</div>" + "</div>");
		
		isNumbersEqual(first, second);
		isBigDifference(first, second);
		total += first + second;
	}
	showTextById(getTotalResult(total));
})();








